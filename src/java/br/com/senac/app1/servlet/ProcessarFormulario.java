/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.app1.servlet;

import org.apache.commons.mail.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ProcessarFormulario", urlPatterns = {"/paginas/processarFormularioSugestao.do"})
public class ProcessarFormulario extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        boolean isInformacoesValidas = false;
        boolean isReceberInformacoes = false;
        String emailx = request.getParameter("email");
        String nome = request.getParameter("nome");
        String assunto = request.getParameter("assunto");
        String mensagem = request.getParameter("mensagem");
        String infovalid = request.getParameter("info");

        String receberInformacoes = new String();
        if (request.getParameter("receberInformacoes") != null) {
            receberInformacoes = "Sim";
        } else {
            receberInformacoes = "Nao";
        }

        System.out.println("Nome: " + nome);
        System.out.println("Assunto: " + assunto);
        System.out.println("Mensagem: " + mensagem);
        System.out.println("As informacoes foram uteis: " + infovalid);
        System.out.println("Receber Informacoes: " + receberInformacoes);

        String msgenvio
                = "<b>Nome:</b> " + nome + "<br></br><br></br>"
                + "<b>Assunto:</b> " + assunto + "<br></br><br></br>"
                + "<b>As informacoes foram uteis:</b> " + infovalid + "<br></br><br></br>"
                + "<b>Receber Informacoes:</b> " + receberInformacoes + "<br></br><br></br>"
                + "<b>Mensagem:</b><br></br> " + mensagem + "<br></br>";

        String msgenviox
                = "Ola," +  "<br></br>"
                + "Recebemos sua solicitação e estaremos atendendo o mais breve possivel." + "<br></br>"
                + "Atenciosamente ," + "<br></br>" 
                + "Equipe Comandos.";

        String myEmailId = "samuelbr.uc14@gmail.com";
        String myPassword = "samucav312";
        String senderId = "samuelbr.uc14@gmail.com";
        try {
            HtmlEmail email = new HtmlEmail();
            email.setSmtpPort(587);
            email.setAuthenticator(new DefaultAuthenticator(myEmailId, myPassword));
            email.setDebug(true);
            email.setHostName("smtp.gmail.com");
            email.setFrom(myEmailId);
            email.setSubject("UC14 - Luis Gustavo Loureiro Fernandes");
            email.setHtmlMsg(msgenvio);
            email.addTo(senderId);
            email.setTLS(true);

            /*
            EmailAttachment attachment = new EmailAttachment();
            attachment.setPath("/caminho/tables.xlsx");
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("Excel");
            attachment.setName("tables.xlsx");
            email.attach(attachment);
             */
            email.send();
            System.out.println("E-mail enviado!");
        } catch (Exception e) {
            System.out.println("Exception :: " + e);
        }

        
        String senderIdx = emailx;
        try {
            HtmlEmail email = new HtmlEmail();
            email.setSmtpPort(587);
            email.setAuthenticator(new DefaultAuthenticator(myEmailId, myPassword));
            email.setDebug(true);
            email.setHostName("smtp.gmail.com");
            email.setFrom(myEmailId);
            email.setSubject("UC14 - Luis Gustavo Loureiro Fernandes");
            email.setHtmlMsg(msgenviox);
            email.addTo(senderIdx);
            email.setTLS(true);

            /*
            EmailAttachment attachment = new EmailAttachment();
            attachment.setPath("/caminho/tables.xlsx");
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("Excel");
            attachment.setName("tables.xlsx");
            email.attach(attachment);
             */
            email.send();
            System.out.println("E-mail enviado!");
        } catch (Exception e) {
            System.out.println("Exception :: " + e);
        }

    }

}
